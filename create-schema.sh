#!/bin/bash

THIS=`readlink -e $0`
WD=`dirname $THIS`
. $WD/oracle.conf


echo "Connecting to $ORACLE_USER/$ORACLE_PASSWORD@$ORACLE_HOST:$ORACLE_PORT/$ORACLE_SID ..."

sqlplus $ORACLE_USER/$ORACLE_PASSWORD@$ORACLE_HOST:$ORACLE_PORT/$ORACLE_SID  << EOF
drop table USER_DATA;
create table USER_DATA (
  LOGIN varchar2(32) not null,
  PASSWORD_HASH varchar2(128) not null,
  CURRENT_BALANCE number not null,
  constraint USER_DATA_PRK primary key (LOGIN)
);

exit

EOF

echo "Done."
