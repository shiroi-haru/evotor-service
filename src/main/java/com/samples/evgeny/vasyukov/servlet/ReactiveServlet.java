package com.samples.evgeny.vasyukov.servlet;

import java.io.IOException;

import com.samples.evgeny.vasyukov.App;
import com.samples.evgeny.vasyukov.model.Processor;
import com.samples.evgeny.vasyukov.model.Responses;
import com.samples.evgeny.vasyukov.model.Util;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import rx.Observable;
import rx.schedulers.Schedulers;

import javax.servlet.annotation.WebServlet;
import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;

@WebServlet(urlPatterns = "/reactive", asyncSupported = true)
public class ReactiveServlet extends HttpServlet {
  private final static Logger logger = LoggerFactory.getLogger(ReactiveServlet.class);

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    final long startNano = System.nanoTime();
    logger.debug("Evotor async servlet request received by thread-{}", Thread.currentThread().getId());

    final Processor proc = new Processor(App.JDBC_POOL);

    final AsyncContext asyncContext = req.startAsync();
    executeAsynchronously(asyncContext, proc).subscribeOn(Schedulers.io()).subscribe((o) -> {
      final long now = System.nanoTime();
      logger.debug("Evotor async servlet request processed by thread-{} in [{}]nanos, actual thread time:[{}]nanos",
          Thread.currentThread().getId(), (now-startNano), o);
      asyncContext.complete();
    }, (e) -> {
      logger.error("An error occurred during the client's request processing:", e);
      try {
        asyncContext.getResponse().reset();
        Util.marshall(Responses.technicalError(), asyncContext.getResponse().getWriter());
      } catch (IOException | JAXBException e1) {
        logger.error("Unable to write error response to the client:", e1);
      } finally {
        asyncContext.complete();
      }
    });

    logger.debug("Evotor async servlet dispatching took [{}]ms", System.nanoTime() - startNano);
  }

  private Observable<Long> executeAsynchronously(AsyncContext asyncContext, Processor proc) {
    return Observable.fromCallable(() -> {
      final long startNano = System.nanoTime();
      proc.handleRequest(asyncContext.getRequest().getReader(), asyncContext.getResponse().getWriter());
      return System.nanoTime() - startNano;
    });
  }
}
