package com.samples.evgeny.vasyukov;

import org.apache.catalina.webresources.AbstractResourceSet;
import org.apache.catalina.webresources.DirResourceSet;
import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.JarResourceSet;
import org.apache.catalina.webresources.StandardRoot;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

import java.io.*;
import java.util.Properties;

public class App {
  final static Properties CONFIG = loadConfig(ClassLoader.getSystemResourceAsStream("oracle.conf"));
  public final static DataSource JDBC_POOL = pool(CONFIG);

  public static void main(String[] args) throws Exception {
    startServer(CONFIG);
  }

  private static void startServer(final Properties props) throws Exception {
    final Tomcat tomcat = new Tomcat();
    tomcat.setPort(Integer.valueOf(props.getProperty("LISTENING_PORT", "8080")));
    final StandardContext ctx = (StandardContext) tomcat.addWebapp("/", new File(".").getAbsolutePath());
    final WebResourceRoot resources = new StandardRoot(ctx);
    final File source = new File(App.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
    final AbstractResourceSet resource = source.isFile() ?
        new JarResourceSet(resources, "/WEB-INF/classes", source.getAbsolutePath(), "/") :
        new DirResourceSet(resources, "/WEB-INF/classes", source.getAbsolutePath(), "/");
    resources.addPreResources(resource);
    ctx.setResources(resources);

    final Logger root = (Logger)LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    root.setLevel(Level.toLevel(props.getProperty("LOG_LEVEL", "DEBUG")));

    pool(props);
    tomcat.start();
    tomcat.getServer().await();
  }

  private static Properties loadConfig(InputStream config) {
    final Properties prop = new Properties();
    try {
      try (final InputStream input = new BufferedInputStream(config)) {
        prop.load(input);
      }
    } catch (IOException e) {
      throw new IllegalStateException("Unable to load attached config file!");
    }
    return prop;
  }

  synchronized private static DataSource pool(final Properties props) {
    final PoolProperties p = new PoolProperties();
    String sb = "jdbc:oracle:thin:@" + props.getProperty("ORACLE_HOST") + ':' +
        props.getProperty("ORACLE_PORT") + ':' + "ORA11";
    p.setUrl(sb);
    p.setDriverClassName("oracle.jdbc.driver.OracleDriver");
    p.setUsername(props.getProperty("ORACLE_USER"));
    p.setPassword(props.getProperty("ORACLE_PASSWORD"));
/*
    p.setUsername("JENYA");
    p.setPassword("Kinkakuji");
*/
    p.setJmxEnabled(true);
    p.setTestWhileIdle(false);
    p.setTestOnBorrow(true);
    p.setValidationQuery("select 1 from dual");
    p.setTestOnReturn(false);
    p.setValidationInterval(30000);
    p.setTimeBetweenEvictionRunsMillis(30000);
    p.setMaxActive(100);
    p.setInitialSize(10);
    p.setMaxWait(10000);
    p.setRemoveAbandonedTimeout(60);
    p.setMinEvictableIdleTimeMillis(30000);
    p.setMinIdle(10);
    p.setLogAbandoned(true);
    p.setRemoveAbandoned(true);
    p.setJdbcInterceptors("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;" +
        "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
    final DataSource pool = new DataSource();
    pool.setPoolProperties(p);
    return pool;
    //setPool(pool);
/*    Connection con = null;
    try {
      con = pool.getConnection();
      Statement st = con.createStatement();
      ResultSet rs = st.executeQuery("select * from user_data");
      int cnt = 1;
      while (rs.next()) {
        System.out.println((cnt++) + ". Host:" + rs.getString("Host") +
            " User:" + rs.getString("User") + " Password:" + rs.getString("Password"));
      }
      rs.close();
      st.close();
    } finally {
      if (con != null) try {
        con.close();
      } catch (Exception ignore) {
      }
    }*/

  }
}
