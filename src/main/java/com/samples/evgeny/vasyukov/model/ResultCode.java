package com.samples.evgeny.vasyukov.model;

public enum ResultCode {
  OK((short)0), INVALID_PASSWORD((short)1);

  private final short code;

  ResultCode(short code) {
    this.code = code;
  }
}
