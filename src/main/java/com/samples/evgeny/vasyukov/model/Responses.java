package com.samples.evgeny.vasyukov.model;

import java.math.BigDecimal;
import java.util.stream.Collectors;

public class Responses {
  private final static Response USER_CREATED = new Response();
  private final static Response ALREADY_EXISTS = new Response();
  private final static Response TECHNICAL_ERROR = new Response();
  private final static Response NOT_EXISTS = new Response();
  private final static Response WRONG_PASSWORD = new Response();

  static {
    USER_CREATED.setResultCode((short)0);
    ALREADY_EXISTS.setResultCode((short)1);
    TECHNICAL_ERROR.setResultCode((short)2);
    NOT_EXISTS.setResultCode((short)3);
    WRONG_PASSWORD.setResultCode((short)4);
  }

  private static Response copy(Response response) {
    final Response result = new Response();
    result.setResultCode(response.getResultCode());
    result.getExtra().addAll(response.getExtra().stream().map((ex) -> {
      final Response.Extra extra = new Response.Extra();
      extra.setName(ex.getName());
      extra.setValue(ex.getValue());
      return extra;
    } ).collect(Collectors.toList()));
    return result;
  }

  public static Response okBalanceResponse(BigDecimal balance) {
    final Response response = new Response();
    final Response.Extra extra = new Response.Extra();
    extra.setName(ResponseExtraTypeType.BALANCE);
    extra.setValue(balance.toPlainString());
    response.getExtra().add(extra);
    response.setResultCode((short)0);
    return response;
  }


  public static Response userCreated() {
    return copy(USER_CREATED);
  }

  public static Response alreadyExists() {
    return copy(ALREADY_EXISTS);
  }

  public static Response technicalError() {
    return copy(TECHNICAL_ERROR);
  }

  public static Response notExists() {
    return copy(NOT_EXISTS);
  }

  public static Response wrongPassword() {
    return copy(WRONG_PASSWORD);
  }
}
