package com.samples.evgeny.vasyukov.model;

import com.google.common.base.Charsets;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.Reader;
import java.io.Writer;

public class Util {
  private static final String EVOTOR_XML_SCHEMA = "evotor-test-task.xsd";
  private static final HashFunction SHA512 = Hashing.sha512();

  public static <T> T unmarshall(final Class <T> docClass, final Reader is) throws JAXBException {
    JAXBContext jc = JAXBContext.newInstance(docClass);
    javax.xml.bind.Unmarshaller u = jc.createUnmarshaller();
    SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
    Schema schema;
    try {
      schema = schemaFactory.newSchema(ClassLoader.getSystemResource(EVOTOR_XML_SCHEMA));
    } catch (SAXException e) {
      throw new NotParsableRequestException("unable to parse request as xml: ", e);
    }
    u.setSchema(schema);
    return docClass.cast(u.unmarshal(is));
  }

  public static <T> void marshall(final T response, final Writer os) throws JAXBException {
    final JAXBContext jc = JAXBContext.newInstance("com.samples.evgeny.vasyukov.model");
    final Marshaller m = jc.createMarshaller();

    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    m.marshal(response, os);
  }

  public static String passwordHash(String password) {
    return SHA512.hashBytes(password.getBytes(Charsets.UTF_8)).toString();
  }
}
