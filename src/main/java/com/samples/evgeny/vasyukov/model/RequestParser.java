package com.samples.evgeny.vasyukov.model;

import javax.xml.bind.JAXBException;
import java.io.Reader;
import java.io.Writer;
import java.util.*;
import java.util.stream.Collectors;

class RequestParser {
  public static ParsedRequest parse(Reader reader) {
    try {
      final Request request = Util.unmarshall(Request.class, reader);
      final List <String> logins = request.getExtra().stream().filter((x) -> x.getName()==RequestExtraTypeType.LOGIN)
          .map(Request.Extra::getValue).collect(Collectors.toList());
      final List <String> passws = request.getExtra().stream().filter((x) -> x.getName()==RequestExtraTypeType.PASSWORD)
          .map(Request.Extra::getValue).collect(Collectors.toList());
      if (logins.size() != 1 || passws.size() != 1) {
        throw new NotParsableRequestException("Unable to parse request, login & password must be mentioned only once!");
      }
      return new ParsedRequest(logins.get(0), passws.get(0), request.getRequestType());
    } catch (JAXBException e) {
      throw new NotParsableRequestException("Unable to parse request: ", e);
    }
  }

  static void encode(Writer writer, Response response) {
    try {
      Util.marshall(response, writer);
    } catch (JAXBException e) {
      throw new ResponseNotMarshallableException("Unable to write response: ", e);
    }
  }

  static class ParsedRequest {
    private final String login, password;
    private final RequestTypeType requestType;

    private ParsedRequest(String login, String password, RequestTypeType requestType) {
      this.login = login;
      this.password = password;
      this.requestType = requestType;
    }

    String getLogin() {
      return login;
    }

    public String getPassword() {
      return password;
    }

    RequestTypeType getRequestType() {
      return requestType;
    }
  }
}
