package com.samples.evgeny.vasyukov.model;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Reader;
import java.io.Writer;
import java.sql.*;

public class Processor {
  private final static Logger logger = LoggerFactory.getLogger(Processor.class);
  private final DataSource pool;

  public Processor(final DataSource pool) {
    this.pool = pool;
  }

  public void handleRequest(Reader requestReader, Writer responseWriter) {
    RequestParser.encode(responseWriter, processRequest(RequestParser.parse(requestReader)));
  }

  private Response processRequest(RequestParser.ParsedRequest request) {
    final String passwordHash = Util.passwordHash(request.getPassword());
    final StringBuilder query = new StringBuilder();
    try  (final Connection connection = pool.getConnection()) {
      try (final Statement st = connection.createStatement()) {
        if (request.getRequestType() == RequestTypeType.CREATE_AGT) {
          query.append("insert into USER_DATA (LOGIN, PASSWORD_HASH, CURRENT_BALANCE) values ('")
              .append(request.getLogin()).append("', '").append(passwordHash).append("', '").append(0.0).append("')");
          try (final ResultSet ignored = st.executeQuery(query.toString())) {
            connection.commit();
            return Responses.userCreated();
          } catch (SQLIntegrityConstraintViolationException e) {
            logger.info("User {} already exists!", request.getLogin());
            return Responses.alreadyExists();
          }
        } else if (request.getRequestType() == RequestTypeType.GET_BALANCE) {
          query.append("select * from USER_DATA where LOGIN = '").append(request.getLogin()).append("'");
          try (final ResultSet rs = st.executeQuery(query.toString())) {
            if (rs.next()) {
              return passwordHash.equals(rs.getString("PASSWORD_HASH")) ?
                  Responses.okBalanceResponse(rs.getBigDecimal("CURRENT_BALANCE")) :
                  Responses.wrongPassword();
            } else {
              return Responses.notExists();
            }
          }
        } else {
          throw new IllegalStateException("Unexpected request type. Build is broken?!");
        }
      }
    } catch (SQLException e) {
      logger.error("Unable to process request due to storage error:", e);
      throw new RuntimeException(e);
    }
  }
}
