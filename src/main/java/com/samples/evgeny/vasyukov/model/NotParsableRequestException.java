package com.samples.evgeny.vasyukov.model;

public class NotParsableRequestException extends RuntimeException {
  public NotParsableRequestException(String msg, Throwable th) {
    super(msg, th);
  }

  public NotParsableRequestException(String msg) {
    super(msg);
  }
}

