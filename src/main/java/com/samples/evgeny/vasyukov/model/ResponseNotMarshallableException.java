package com.samples.evgeny.vasyukov.model;

public class ResponseNotMarshallableException extends RuntimeException {
  public ResponseNotMarshallableException(String msg, Throwable th) {
      super(msg, th);
    }

  public ResponseNotMarshallableException(String msg) {
    super(msg);
  }
}
